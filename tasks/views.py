from django.shortcuts import render
from tasks.models import Task
from tasks.forms import TaskForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


@login_required()
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "create_task.html", context)


@login_required()
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "mine.html", context)
